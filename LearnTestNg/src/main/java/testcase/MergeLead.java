package testcase;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import com.testng.annotations.Annotations;
//@Ignore --> Even if the class Merge Lead is given in XML file for execution the Merge lead will be ignored (not executed)
public class MergeLead extends Annotations {
	@BeforeTest(groups="regression")
	public void setData()
	{
		testName = "Merge Lead";
		testDescrip = "Merging two leads existing lead";
		author = "Dhanalakshmi";
		category = "Testing";
	}
	//@Ignore --> Ignores the method even when it given in XML
	@Test(groups="regression")
	public void main() {
		WebElement eleLead = locateElement("xpath","//a[text()='Leads']");
		click(eleLead);
		WebElement eleMer = locateElement("xpath","//a[text()='Merge Leads']");
		click(eleMer);
		WebElement eleIcon1 = locateElement("xpath","//img[@src='/images/fieldlookup.gif']");
		click(eleIcon1);
		switchToWindow(1);
		WebElement eleFName = locateElement("xpath","//input[@name='firstName']");
		clearAndType(eleFName,"Mottu");
		WebElement eleBtn = locateElement("xpath","//button[text()='Find Leads']");
		click(eleBtn);
		WebElement eleId = locateElement("link","10481");
		click(eleId);
		switchToWindow(0);
		WebElement eleIcon2 = locateElement("xpath","//input[@id='partyIdTo']//following::img");
		click(eleIcon2);
		switchToWindow(1);
		WebElement eleFNam = locateElement("xpath","//input[@name='firstName']");
		clearAndType(eleFNam,"Mottu");
		WebElement eleBt = locateElement("xpath","//button[text()='Find Leads']");
		click(eleBt);
		WebElement eleTid = locateElement("link","10483");
		click(eleTid);
		switchToWindow(0);
		WebElement eleMg = locateElement("xpath","//a[text()='Merge']");
		click(eleMg);
		acceptAlert();
		//WebElement eleFl = locateElement("xpath","//a[text()='Find Leads']");
		//click(eleFl);
		}

}
