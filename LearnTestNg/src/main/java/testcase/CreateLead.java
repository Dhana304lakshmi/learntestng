package testcase;
//import java.util.List;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.testng.annotations.Annotations;

public class CreateLead extends Annotations{
	//@BeforeTest(groups="smoke") 
	//@BeforeTest(groups="common") 
	@BeforeTest
	public void setData()
	{
		testName = "Create Lead";
		testDescrip = "Creating a new Lead";
		author = "Dhanalakshmi";
		category = "Testing";
		excelFileName = "create";
	}

	//@Test(invocationCount=2)//invocation count specifies number of times the method needs to be executed 
	//Here the create lead method executes 2 times
	//@Test(groups="smoke") to run only smoke test case
	// @Test(groups="smoke")
	//@Test(groups="common")
	
	@Test(dataProvider="fetchData")
	public void createLead(String comp, String fName, String lName) {
		//WebElement eleCrLead = locateElement("xpath", "//a[text() ='Create Lead']");
		//click(eleCrLead);
		click(locateElement("xpath", "//a[text() ='Create Lead']"));
		clearAndType(locateElement("id","createLeadForm_companyName"),comp);
		clearAndType(locateElement("xpath","//input[@id='createLeadForm_firstName'][1]"),fName);
		clearAndType(locateElement("xpath","//input[@id='createLeadForm_lastName'][1]"),lName);
//		WebElement eleInds = locateElement("id","createLeadForm_industryEnumId");
//		Select dd = new Select(eleInds);
//		List<WebElement> drwn = dd.getOptions();
//		int siz = drwn.size();
//		dd.selectByIndex(siz-1);
//		WebElement eleOwnr = locateElement("id","createLeadForm_ownershipEnumId");
//		Select dd1 = new Select(eleOwnr);
//		dd1.selectByVisibleText("Corporation");
//		WebElement eleSrc = locateElement("id","createLeadForm_dataSourceId");
//		Select dd2 =  new Select(eleSrc);
//		dd2.selectByIndex(1);
		WebElement eleSubmit = locateElement("name","submitButton");
		click(eleSubmit);

	}
}

