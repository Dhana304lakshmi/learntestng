package testcase;

import org.openqa.selenium.WebElement;
import com.testng.annotations.Annotations;

public class DeleteLead extends Annotations {	
	public void delete()
	{
		WebElement eleLead = locateElement("xpath","//a[text()='Leads']");
		click(eleLead);
		WebElement eleFind = locateElement("xpath","//a[text()='Find Leads']");
		click(eleFind);
		WebElement eleName = locateElement("xpath","(//input[@name = 'firstName'])[3]");
		clearAndType(eleName,"Ragvi");
		WebElement eleClick = locateElement("xpath","//button[text()='Find Leads']");
		click(eleClick);
		WebElement usId = locateElement("xpath","//a[text()='10623']");
		click(usId);
		WebElement del = locateElement("xpath","//a[text()='Delete']");
		click(del);
		
	}
}
