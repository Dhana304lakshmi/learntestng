package learnExcel;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ReadExcel {
	private static XSSFWorkbook wbook;
	// TO READ ALL THE DATA IRRESPECTIVE OF NUMBER OF ROWS AND COLUMNS
	public static void main(String[] args) {
		try 
		{
			wbook = new XSSFWorkbook("./data/create.xlsx");
			XSSFSheet sheet = wbook.getSheet("Sheet1"); // To read the given sheet in the workbook
			int rowCount = sheet.getLastRowNum(); // To get number of rows(data) given in the sheet (last row of data)
			System.out.println("Number of rows: "+rowCount); 
			short colCount = sheet.getRow(0).getLastCellNum(); // To get the number of columns with respect to column in the given sheet
			System.out.println("Number of Columns :"+colCount);
			for (int i = 1; i<=rowCount; i++) // To get data in row dynamically  based on the row count
			{
				XSSFRow row = sheet.getRow(i);	
				for(int j=0; j<colCount; j++) // To get data in the column dynamically based on the column count
				{
					XSSFCell col = row.getCell(j);
					String cellValue = col.getStringCellValue(); //To get the value of given cell 
					System.out.println(cellValue);
				}
			}
		}
		catch (IOException e)  
		{
			System.out.println("IO Exception");
			//IllegelStateException --> When the excel sheet has data different data type(Eg.Interger) and we fetch data in String
		}
	}
}
/* **** TO READ THE DATA WITH STATIC INPUT OF ROWS AND COLUMNS ****** */
/*public static void main(String[] args) {
try 
{
	XSSFWorkbook wbook = new XSSFWorkbook("./data/create.xlsx");
	//XSSFSheet sheet = wbook.getSheetAt(0);
	XSSFSheet sheet = wbook.getSheet("Sheet1");
	XSSFRow row = sheet.getRow(1);
	XSSFCell col = row.getCell(1);
	String cellValue = col.getStringCellValue();
	System.out.println(cellValue);
} 

catch (IOException e) 
{
	System.out.println("IO Exception");
}

}*/