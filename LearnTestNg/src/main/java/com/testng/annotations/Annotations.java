package com.testng.annotations;
import com.testng.selenium.api.base.SeleniumBase;

import Learning.ReadExcel;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase{
 // @BeforeMethod(groups="smoke") to run only smoke test case
 //	@BeforeMethod(groups="sanity") when needed only for smoke test
// @BeforeMethod(groups="common")//common method for all the test cases hence changing the name as common(self explanatory)
  @Parameters({"url","username","password"})
  @BeforeMethod
  public void beforeMethod(String url,String username, String password) {
	    startApp("chrome", url);
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmSfa = locateElement("xpath","//a[contains(text() ,'CRM/SFA')]");
		click(eleCrmSfa);
  }
  
  @DataProvider(name="fetchData")
	public Object[][] fetchData()
	{
		return ReadExcel.readExcelSheet(excelFileName);
	  
	}

 // @AfterMethod(groups="smoke") to run only smoke test case
 //@AfterMethod(groups="sanity")
//@AfterMethod(groups="common")//common method for all the test cases hence changing the name as common(self explanatory)
  @AfterMethod
  public void afterMethod() {
	  close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
