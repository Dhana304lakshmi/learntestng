package com.project.facebook;
import com.testng.selenium.api.base.SeleniumBase;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
public class FacebookTest extends SeleniumBase{
	
	@BeforeClass
	public void setData()
	{
		testName = "Facebook";
		testDescrip = "Searching testleaf";
		author = "Dhanalakshmi";
		category = "Testing";
	}
	
	@BeforeMethod
	  public void beforeMethod() {
		    startApp("chrome", "https://www.facebook.com/");
		    clearAndType(locateElement("xpath", "//input[@id='email']"),"");
		    clearAndType(locateElement("xpath", "//input[@id='pass']"),"");
		    click(locateElement("xpath","//input[@value='Log In']"));  
	  }

  @AfterMethod
  public void afterMethod() {
	close();
  }

}
