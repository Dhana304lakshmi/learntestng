package Learning;
import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	ExtentTest test;
	public String testName, testDescrip, author, category;
	public static String excelFileName;
	//@BeforeSuite(groups="smoke") 
	//@BeforeSuite(groups="sanity") 
	//@BeforeSuite(groups="common") //I need this method for both smoke and sanity
	@BeforeSuite
	public void startReport()
	{
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(reporter);	
	}
	
	//@BeforeMethod(groups="smoke")
	//@BeforeMethod(groups="sanity")
	//@BeforeMethod(groups="common") //I need this method for both smoke and sanity
	@BeforeMethod
	public void report() throws IOException 
	{
		test = extent.createTest(testName, testDescrip);	
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void reportStep (String desc, String status)
	{
	if(status.equalsIgnoreCase("pass"))	
	{
		test.pass(desc);
	}
	else if(status.equalsIgnoreCase("fail"))
	{
		test.fail(desc);
	}
	}
	
//	@AfterSuite(groups="smoke")
	//@AfterSuite(groups="sanity")
	//@AfterSuite(groups="common") //I need this method for both smoke and sanity
	@AfterSuite
	public void stopReport()
	{
		extent.flush();
	}

}
