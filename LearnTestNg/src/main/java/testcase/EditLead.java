package testcase;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.testng.annotations.Annotations;
public class EditLead extends Annotations {
	//@BeforeTest(groups="sanity")
	//@BeforeTest(groups="common")
	@BeforeTest
	public void setData()
	{
		testName = "Edit Lead";
		testDescrip = "Editing an existing lead";
		author = "Dhanalakshmi";
		category = "Testing";
	}
	//@Test (dependsOnMethods = {"testcase.CreateLead.createLead"}) // Edit lead method depends on the create lead method
	//When the execution of create lead test case failed, the Edit lead will not run and it shows the status as SKIP
	//@Test (dependsOnMethods = {"testcase.CreateLead.createLead"},alwaysRun = true) // alwaysRun --> Even the Create lead test case fails
	//the Edit lead will execute (either pass or fail) will not skip
	//@Test(timeOut = 5000)//Edit lead has to complete within the given time else it should be failed
	// @Test(groups="sanity",dependsOnMethods = {"testcase.CreateLead.createLead"})
	//@Test(groups="common")
	@Test
	public void edit()
	{
		click(locateElement("xpath","//a[text()='Leads']"));
		click(locateElement("xpath","//a[text()='Find Leads']"));
		clearAndType(locateElement("xpath","(//input[@name = 'firstName'])[3]"),"Mottu");
		click(locateElement("xpath","//button[text()='Find Leads']"));
		click(locateElement("xpath","//a[text()='10481']"));
		click(locateElement("xpath","//a[text()='Edit']"));
		clearAndType(locateElement("id","updateLeadForm_generalProfTitle"),"Testing title");
		click(locateElement("id","updateLeadForm_birthDate-button"));
		
	}

}
